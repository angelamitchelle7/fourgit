#!/bin/zsh

echo  "set up and configure servers"

filename=config.yaml 
#This is a variable that will parse config.yaml to te script each time "filename appears"
# Introducing conditionals
config_folder=$1
if  [ -d "config_folder" ] #This is to ask shell to check if "Config_folder" is a directory (-d) and if yes, it should output the list of the config into the variable "configfiles" 
then 
	echo "Reading config directory..."
	sleep 1 #This is to make the script wait for one second before outputng the list of the config directory.
	configfiles=$(ls "config_folder") 
else
	echo "Config directory not found. Creating one ..." #The else command shows what will happen if the "IF" line is not true and creates a config drectory. 
	mkdir "$config_folder"
	touch "$config_folder/newfile"
fi
sleep 2

echo "Using $filename, configure something"
echo "This is the content of the config_folder directory $configfiles"

#add another conditionals to check the usergroup and leave a message if the userdroup parsed matches with the one provided in the conditionals

usergroup=$2

if [ "$usergroup" == "diogo" ]
then
	echo "Hello diogo please configure the server"
elif [ "$usergroup" == "gela" ]
then
	echo "Hi gela, you have the permission to configure the server"
else
	echo "Wrong User!....."
	sleep 2
	echo "Permission denied"
fi
